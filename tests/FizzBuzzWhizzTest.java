import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class FizzBuzzWhizzTest {
    FizzBuzzWhizz fbw;
    int num;
    String expectedResult;
    
    @Before
    public void setUp() throws Exception {
        fbw = new FizzBuzzWhizz();
    }
    @After
    public void tearDown() throws Exception {
    }
//  R5: If number is prime, return “whizz” 
//  R6 If number meets (R5) AND (R1, R2, or R3) - append “whizz” to end of string
//  Example: 
//  1 returns “1”
//  2 returns “whizz”
//  3 returns “fizzwhizz”
//  4 returns “4”
//  5 returns “buzzwhizz”
//  6 returns “6”
//  7 returns “whizz”
//  9 returns “fizz”
//  10 returns “buzz”
//  15 returns “fizzbuzz”
//  
//  //R0:
//  
//  public void testFunctionAcceptsNumberGreaterThanZero() {
//      assertEquals("Error",fbw.greaterThanZero());
//  }
    
//  R1: Return "fizz" if the number is divisible by 3
    @Test
    public void testDivisibilityByThree() {
        num = 12;
        expectedResult = "fizz";
        assertEquals(expectedResult,fbw.divisibilityCheck(num));
    }
    
//  R2:Return "buzz" if the number is divisible by 5
    @Test
    public void testDivisibilityByFive() {
        num = 10;
        expectedResult = "buzz";
        assertEquals(expectedResult, fbw.divisibilityCheck(num));
    }
    
//  R3:Return "buzz" if the number is divisible by 3 & 5
    @Test
    public void testDivisibilityBy3or5() {
        num = 45;
        expectedResult = "whizz";
        assertEquals(expectedResult,fbw.divisibilityCheck(num));
    }
    
//  R4: Return the number if no other requirement is fulfilled. The numbers must be returned as a string.
    @Test
    public void testNumberDoesNotMetOtherConditions() {
        int num = 4;
        assertEquals("4",fbw.divisibilityCheck(num));
    }
}